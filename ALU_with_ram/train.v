module xintoy(out,x,y);

output reg[31:0]out;
input [15:0]x,y;

reg [15:0]out_hi,out_low;
reg c;

integer i;

always@(*)
begin
	out=0;
	out_hi=16'b0;

	#1;
	out_low=x;
	for(i=0;i<16;i=i+1)begin
		c=0;
		if(out_low[0])begin
			{c,out_hi}=out_hi+y;
		end		
		{out_hi,out_low}={c,out_hi,out_low}>>1;

	end
	out={out_hi,out_low};
end

endmodule

module xdivy(out,x,y);
output reg[15:0]out;

input [15:0]x,y;

integer i;

reg [15:0]temp;

always@(*)
begin

	{temp,out}=x;
	for(i=0;i<16;i=i+1)begin
		{temp,out}={temp,out}<<1;
		if(temp>=y)begin
			out[0]=1'b1;
			temp=temp-y;
		end

	end

end

endmodule
