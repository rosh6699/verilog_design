module dlatch(q,qbar,d,en);
input d,en;
output q,qbar;

nand(p,d,en);
not(dbar,d);
nand(i,dbar,en);

nand(q,p,qbar);
nand(qbar,i,q);

endmodule


module dff(q,qbar,d,clk);

input d,clk;
output q,qbar;

not(clkbar,clk);
dlatch d1(a,,d,clkbar);
dlatch d2(q,qbar,a,clk);

endmodule

module mux_2x1(out,in0,in1,sel);
input in0,in1,sel;
output out;

not(selbar,sel);
and(a,in0,selbar);
and(b,in1,sel);
or(out,a,b);

endmodule

module binarycell(out,in,load,clk);

input in,load,clk;
output out;
mux_2x1 m1(p,out,in,load);

dff d1(out,,p,clk);

endmodule

module mux_8x1(out,in0,in1,in2,in3,in4,in5,in6,in7,sel0,sel1,sel2);
input in0,in1,in2,in3,in4,in5,in6,in7,sel0,sel1,sel2;
output out;


mux_2x1 m1(p,in0,in1,sel0);
mux_2x1 m2(q,in2,in3,sel0);
mux_2x1 m3(r,in4,in5,sel0);
mux_2x1 m4(s,in6,in7,sel0);

mux_2x1 m5(m,p,q,sel1);
mux_2x1 m6(n,r,s,sel1);

mux_2x1 m7(out,m,n,sel2);

endmodule

module dmux_1x2(out0,out1,in,sel);
input in,sel;
output out0,out1;
not(selbar,sel);
and(out0,in,selbar);
and(out1,in,sel);
endmodule


module dmux_1x8(out0,out1,out2,out3,out4,out5,out6,out7,in,sel0,sel1,sel2);
input in,sel0,sel1,sel2;
output out0,out1,out2,out3,out4,out5,out6,out7;

dmux_1x2 d1(p1,p2,in,sel2);

dmux_1x2 d3(p3,p4,p1,sel1);
dmux_1x2 d4(p5,p6,p2,sel1);

dmux_1x2 d5(out0,out1,p3,sel0);
dmux_1x2 d6(out2,out3,p4,sel0);
dmux_1x2 d7(out4,out5,p5,sel0);
dmux_1x2 d8(out6,out7,p6,sel0);

endmodule



