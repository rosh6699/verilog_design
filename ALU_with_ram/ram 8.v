module ram8(out,in,sel0,sel1,sel2,load,clk);
input sel0,sel1,sel2,load,clk;
output [15:0]out;
input [15:0]in;

dmux_1x8 d1(o0,o1,o2,o3,o4,o5,o6,o7,load,sel0,sel1,sel2);
wire [15:0]i0,i1,i2,i3,i4,i5,i6,i7;

binarycell b1[15:0](i0,in,o0,clk);
binarycell b2[15:0](i1,in,o1,clk);
binarycell b3[15:0](i2,in,o2,clk);
binarycell b4[15:0](i3,in,o3,clk);
binarycell b5[15:0](i4,in,o4,clk);
binarycell b6[15:0](i5,in,o5,clk);
binarycell b7[15:0](i6,in,o6,clk);
binarycell b8[15:0](i7,in,o7,clk);

mux_8x1 m1[15:0](out,i0,i1,i2,i3,i4,i5,i6,i7,sel0,sel1,sel2);

endmodule

module xy(out,x,y);
output reg[31:0]out;
input [15:0]x,y;
reg [15:0]outlo,outhi;
reg c;
integer i;
always@(*) 
begin
out=0;
outhi=16'b0;
#1;
outlo=y;
for(i=0;i<16;i=i+1)begin
c=0;
if(outlo[0])begin
{c,outhi}=outhi+x;
end
{outhi,outlo} = {c,outhi,outlo}>>1;
end
out = {outhi,outlo};
end
endmodule


module xplusy(out,x,y);

input [15:0]x,y;
output reg[15:0]out;
always@(*)
begin
	out=x+y;
end

endmodule

module xbyy(out,x,y);

input [15:0]x;
input [15:0]y;

output reg[15:0]out;

reg [15:0]temp;

integer i;
always@(*)
begin
	{temp,out}=x;
	for(i=0;i<16;i=i+1)begin
	  {temp,out}={temp,out}*2;
	   if(temp>=y)begin

		out[0]=1'b1;
		temp=temp-y;
	    end	
	end
end



endmodule


module store(out,x,y);
input[15:0]x,y;
output [31:0]out;

wire [15:0]out0;
reg s2,s1,s0,load,clk;
reg [15:0]in;
reg [15:0]xm,ym;
xy x1(out,xm,ym);
ram8 r1(out0,in,s0,s1,s2,load,clk);

initial
begin
	s2=0;s1=0;s0=0;load=1;in = x;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;	
	
	
	
	s2=0;s1=0;s0=1;in = y;load=1;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;

	load=0;s2=0;s1=0;s0=0;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	xm=out0;
	
	
	load=0; s2=0;s1=0;s0=1;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	ym=out0;


	ad=0; s2=0;s1=1;s0=0;load=1;in=out;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	clk = 1'b0;#2;clk = 1'b1;#2;clk = 1'b0;#2;
	

end
endmodule
